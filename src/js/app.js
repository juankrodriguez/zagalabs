import '../scss/styles.scss'


(function(){
    console.log('hello world zagalabs app!!!');
    var menu = {"items":[
      {"title":"eventos","link":"eventos"},
      {"title":"noticias","link":"noticias"},
      {"title":"interes","link":"interes"},
      {"title":"proximos estrenos","link":"estrenos"},
      {"title":"contactenos","link":"contactenos"},
    ]};

    createMenu( menu.items );
    addEventsTomenu();
    showMenu();
})();

function createMenu( menu ) {
  var menuCont= document.getElementById('menu');
   
   for(var i=0; i<menu.length;i++)
   {
      var item = document.createElement("li");
      item.appendChild(document.createTextNode(menu[i].title));
      item.id = menu[i].link;
      item.className = "menuelement";
      menuCont.appendChild(item);
   }
};
function addEventsTomenu()
{
  var element = document.querySelectorAll('.menuelement');
  for (var i = 0; i < element.length; i++) { 
    element[i].addEventListener("click", function(e) {
      var section = e.target.id +"sec";

      var sect = document.getElementById(section);
      var x = sect.offsetLeft, y = sect.offsetTop;
      scrollPage(x,y,400);
    }, false);
  }
}

function scrollPage (x, y, duration) {
  var initX = window.scrollX || window.pageXOffset,
    startY = window.scrollY || window.pageYOffset,
    xPos = x - initX,
    yPos = y - startY,
    initTime = new Date().getTime();

  duration = typeof duration !== 'undefined' ? duration : 400;
  var timer = window.setInterval(function() {
    var time = new Date().getTime() - initTime,
      newX = newPos(time, initX, xPos, duration),
      newY = newPos(time, startY, yPos, duration);
    if (time >= duration) {
      window.clearInterval(timer);
    }
    window.scrollTo(newX, newY);
  }, 1000 / 60); // 60 fps
};

function newPos(time, from, distance, duration) {
  if ((time /= duration / 2) < 1) return distance / 2 * time * time * time * time + from;
  return -distance / 2 * ((time -= 2) * time * time * time - 2) + from;
};

function showMenu() {
  var menu = document.querySelector('.respMenu');
  menu.addEventListener("click", function(e) {
    var menuDisp = document.getElementById("menu").style;
    if(menuDisp.display == 'block') {
      menuDisp.display='none';
    }
    else{
      menuDisp.display = 'block';
    }
  });
}